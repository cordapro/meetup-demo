import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

import About from '@/views/About'
import ArticleForm from '@/views/article/ArticleForm'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: '',
      component: Home,
      children: [
        {
          path: '/',
          name: 'Article Form',
          component: ArticleForm
        },
        {
          path: '/about',
          name: 'About',
          component: About
        }
      ]
    }
  ]
})
